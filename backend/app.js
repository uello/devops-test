const app = require('express')();
const mysql = require("mysql2");
const bodyParser = require('body-parser');

const port = process.env.PORT || 9090;

const connection = mysql.createConnection({
  host: process.env.MYSQL_HOST || "localhost",
  user: process.env.MYSQL_USER || "root",
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DB || "backend"
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/api', (req, res) => {
  res.json({ hello: "from backend api" });
});

app.post('/api/phonebook', (req, res) => {
  try {
    const sql = "INSERT INTO phonebook (name, phone) VALUES (?, ?)";

    connection.query(sql, [req.body.name, req.body.phone], function (err) {
      if (err) throw err;
    });
  } catch (err) {
    conn.end();
    return res.send({
      status: 500,
    });
  }

  res.send({
    status: 200,
  });
});

app.get('/api/phonebook', (req, res) => {
  try {
    connection.query('SELECT * FROM phonebook', (err, results, fields) => {
      if (err) throw err;

      return res.send(results);
    });
  } catch (err) {
    conn.end();
    return res.send({
      status: 500,
    });
  }
});

app.listen(port, () => {
  console.info("Starting backend on port " + port);
});

