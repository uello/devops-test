# Backend

Use Node.js 15

Instale as dependências:
```
$ npm install
```

Configure a aplicação:
```
export MYSQL_HOST = [ database host ]
export MYSQL_USER = [ database user ]
export MYSQL_PASSWORD = [ database password ]
export MYSQL_DB = my_db
```

Rode o app localmente:
```
$ node app.js
```