CREATE DATABASE IF NOT EXISTS phonebook;
USE phonebook;
CREATE TABLE phonebook.phonebook (
    ID int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
    phone VARCHAR(32)
)