# Uello DevOps

__Por favor leia as instruções atentamente__

Existem quatro diretórios nesse repositório:

- **frontend**: Uma simples implementação de agenda de contatos (html/jquery), possui um formulário que aceita nome e telefone e deve comunicar com o backend para salvar o mesmo.
- **backend**: Uma aplicação Node JS que expõe a API para o frontend e salva tudo que recebe em um banco de dados MySQL.
- **mysql**: Contem um dump inicial do banco de dados necessário para a aplicação funcionar.
- **infrastructure**: Vazio por enquanto, mas você pode adicionar aqui algum arquivo específico de infraestrutura, talvez algum arquivo Terraform, Ansible, Chef ou scripts bash.

# Problema

- Faça deploy dessa stack de uma forma com que um contato inserido no frontend seja salvo pelo backend no banco de dados.
- O frontend deve ser servido na raíz do server (ex: http://127.0.0.1) e o backend deve ser servido em `/api` (ex: http://127.0.0.1/api).
- Faça um script que salve um backup do banco de dados para alguma solução de storage cloud (S3, GCS, etc)
- Caso se aplique salve os arquivos de criação do bucket dentro do diretório `infrastructure/`
- Caso queira modificar alguma coisa nas aplicações para torna-las prontas para produção fique a vontade, porém não é um requisito.

# Solução

Nós aceitaremos qualquer um desses tipos de solução:
- Um script utilizando CLI/SDK/API ou biblioteca que faça deploy de todo o projeto em alguma distribuição Linux ou AWS/GCP.
- Um arquivo docker-compose ou algo similar que rode todo o projeto em uma distribuição Linux ou ou AWS/GCP.

**Importante:** edite esse `README.md` com o passo a passo de como fazer deploy do projeto, fique a vontade para adicionar comentários sobre a sua implementação que possam ajudar na hora da avaliação.

# Expectativas

Quando avaliarmos esse teste, levaremos os seguintes pontos em consideração:

- Se a solução enviada funciona ou não
- Quão fácil é fazer o deploy do projeto
- Quão resiliente a solução é. (Se o banco de dados demorar mais que o normal para iniciar, o que acontece com a aplicação? Quebra e nunca se recupera?)

Imagine que um desenvolvedor júnior que possui acesso a uma distribuição Linux e ao GCP/AWS, ele conseguiria rodar sua solução? Conseguiria identificar problemas rapidamente?

Isso é apenas um teste, não esperamos uma solução pronta para produção, mas esperamos que você mostre que consegue desenvolver uma solução pronta para produção com as ferramentas e o tempo certo.

# Envio

- Por favor, não faça `fork` desse repositório e nem publique sua solução em um repositório aberto.

- Clone o repositório:
```
git clone git@bitbucket.org:uello/devops-test.git
```
- Implemente sua solução
- Faça commit da sua solução
- Crie um arquivo `patch` contendo sua solução:
```
$ git format-patch origin/master --stdout > result.patch
```
- Verifique o arquivo `result.patch` ele deve parecer com o output do comando `git diff`, se tudo estiver certo nos envie o arquivo `result.patch`.

# Contato

Sinta-se à vontade para contatar nosso time caso tenha alguma dúvida!